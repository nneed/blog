<?php

class m161029_181823_create_tag_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('tbl_tag', array(
			'id'=> 'pk',
			'name'=>'string NOT NULL'
		));
	}

	public function down()
	{
		$this->dropTable('tbl_tag');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}