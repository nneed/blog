<?php

class m161029_175035_create_post_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('tbl_post', array(
			'id'=> 'pk',
			'title'=>'string NOT NULL',
			'text'=>'text NOT NULL',
			'tag'=>'integer'
		));
	}

	public function down()
	{
		$this->dropTable('tbl_post');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}