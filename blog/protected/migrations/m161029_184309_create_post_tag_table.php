<?php

class m161029_184309_create_post_tag_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('tbl_post_tag', array(
			'id'=> 'pk',
			'post_id'=>'integer NOT NULL',
			'tag_id'=>'integer NOT NULL'
		));
		$this->addForeignKey('fk_post_id', 'tbl_post_tag', 'post_id',
			'tbl_post', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_tag_id', 'tbl_post_tag', 'tag_id',
			'tbl_tag', 'id', 'CASCADE', 'CASCADE');
	}

	public function down()
	{
		$this->dropTable('tbl_post_tag');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}